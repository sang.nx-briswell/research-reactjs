import React, { Component } from "react";
import axios from "axios";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
} from "react-router-dom";
import Table from "./Component/TableComponent";
import Form from "./Component/FormComponent";
import SideBar from "./Component/SideBarComponent";
import ToggleButton from "./Component/ToggleButtonComponent";
import PersonList from "./Component/PersonListComponent";
import Portfolio from "./Component/PortfolioComponent";
import "./Component/css/sidebar.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.addActiveClass = this.addActiveClass.bind(this);
    this.state = {
      characters: [],
      active: false,
    };
  }
  
  componentDidMount = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        const characters = res.data;
        this.setState({
          characters: characters,
        });
      })
      .catch((error) => console.log(error));
  };

  removeCharacter = (index) => {
    const { characters } = this.state;
    this.setState({
      characters: characters.filter((character) => {
        return character.id !== index;
      }),
    });

    axios
      .delete(`https://jsonplaceholder.typicode.com/users/${index}`)
      .then((res) => {
        console.log(res.status);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  addActiveClass() {
    const { active } = this.state;
    this.setState({
      active: !active,
    });
  };

  child = () => {
    const { id } = useParams();
    return (
      <div>
        <h3>{id}</h3>
      </div>
    );
  };

  handleSubmit = (character) => {
    this.setState({
      characters: [...this.state.characters, character],
    });
  };

  render() {
    const { characters } = this.state;

    return (
      <div className="wrapper">
        <Router>
          <SideBar className={this.state.active} />
          <div id="content">
            <ToggleButton addActiveClass={this.addActiveClass} />
            <div className="container">
              <Switch>
                <Route exact path="/">
                  <Table
                    characterData={characters}
                    removeCharacter={this.removeCharacter}
                  />
                </Route>
                <Route exact path="/home">
                  <Table
                    characterData={characters}
                    removeCharacter={this.removeCharacter}
                  />
                </Route>
                <Route path="/page/:id" children={<this.child />}></Route>
                <Route path="/about">
                  <Form handleSubmit={this.handleSubmit} />
                </Route>
                <Route path="/portfolio">
                  <Portfolio />
                </Route>
                <Route path="/contact">
                  <PersonList />
                </Route>
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;

import React, { Component } from "react";
import { Link } from "react-router-dom";

class SideBar extends Component {
  render() {
    return (
      <nav id="sidebar" className={this.props.className ? "active" : ""}>
        <div className="sidebar-header">Reseach ReactJS</div>
        <ul className="list-unstyled components">
          <p>Dummy Heading</p>
          <li className="active">
          <Link to="/home">Home</Link>
            {/* <ul className="collapse list-unstyled" id="homeSubmenu">
              <li>
                <Link to="/home/1">Home 1</Link>
              </li>
              <li>
                <Link to="/home/2">Home 2</Link>
              </li>
              <li>
                <Link to="/home/3">Home 3</Link>
              </li>
            </ul> */}
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <a
              href="#pageSubmenu"
              data-toggle="collapse"
              aria-expanded="false"
              className="dropdown-toggle"
            >
              Pages
            </a>
            <ul className="collapse list-unstyled" id="pageSubmenu">
              <li>
                <Link to="/page/1">Page 1</Link>
              </li>
              <li>
                <Link to="/page/2">Page 2</Link>
              </li>
              <li>
                <Link to="/page/3">Page 3</Link>
              </li>
            </ul>
          </li>
          <li>
            <Link to="/portfolio">Portfolio</Link>
          </li>
          <li>
            <Link to="/contact">Contact</Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default SideBar;

import React, { Component } from "react";
import axios from "axios";

class Form extends Component {
  initialState = {
    name: "",
    phone: "",
  };

  state = this.initialState;

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  };

  submitForm = () => {
    this.props.handleSubmit(this.state)

    const user = {
      name: this.state.name
    };
    axios.post('https://jsonplaceholder.typicode.com/users', user)
    .then(res => {
      console.log(res.status);
    });
    this.setState(this.initialState)
  }

  render() {
    const { name, phone } = this.state;

    return (
      <form>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            className="form-control"
            type="text"
            name="name"
            id="name"
            value={name}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="phone">phone</label>
          <input
            className="form-control"
            type="text"
            name="phone"
            id="phone"
            value={phone}
            onChange={this.handleChange}
          />
        </div>
        <input
          type="button"
          className="btn btn-primary"
          value="Submit"
          onClick={this.submitForm}
        />
      </form>
    );
  }
}

export default Form;

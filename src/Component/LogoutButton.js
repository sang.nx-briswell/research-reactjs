import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

const LogoutButton = () => {
  const { user, logout, isAuthenticated } = useAuth0();
  console.log(user);
  return (
    isAuthenticated && (
      <>
        <div className="dropdown">
          <img src={user.picture} alt="user" />
          <div className="dropdown-content">
            <li>{user.name}</li>
            <li>{user.email}</li>
            <button className="btn btn-danger" onClick={() => logout()}>
              Log Out
            </button>
          </div>
        </div>
        <div className="user-info">{user.given_name}</div>
      </>
    )
  );
};

export default LogoutButton;

import React, { Component } from "react";
import axios from "axios";

class PersonList extends Component {
  state = {
    persons: [],
  };

  componentDidMount = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        const persons = res.data;
        this.setState({
          persons: persons,
        });
      })
      .catch((error) => console.log(error));
  };

  render() {
    return (
      <ul>
        {this.state.persons.map((person, index) => (
          <li key={index}>{person.name}</li>
        ))}
      </ul>
    );
  }
}

export default PersonList;

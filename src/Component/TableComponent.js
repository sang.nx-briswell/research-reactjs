import React, { Component } from "react";

const TableHeader = () => {
  const Style = {
    width: '40%'
  };
  return (
    <thead className="thead-dark">
      <tr>
        <th style={Style}>Name</th>
        <th style={Style}>Phone</th>
        <th></th>
      </tr>
    </thead>
  );
};

const TableBody = (props) => {
  const rows = props.characterData.map((row, index) => {
    return (
      <tr key={index}>
        <td>{row.name}</td>
        <td>{row.phone}</td>
        <td>
          <button
            className="btn btn-primary"
            onClick={() => props.removeCharacter(row.id)}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  });
  return <tbody className="thead-light">{rows}</tbody>;
};

class Table extends Component {
  render() {
    const { characterData, removeCharacter } = this.props;
    return (
      <table className="table">
        <TableHeader />
        <TableBody
          characterData={characterData}
          removeCharacter={removeCharacter}
        />
      </table>
    );
  }
}

export default Table;

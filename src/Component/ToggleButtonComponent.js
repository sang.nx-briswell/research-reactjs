import React, { Component } from "react";
import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";

class ToggleButton extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <button
            type="button"
            id="sidebarCollapse"
            className="btn btn-info"
            onClick={() => this.props.addActiveClass()}
          >
            <i className="fas fa-align-left"></i>
            <span> Sidebar</span>
          </button>
        </div>
        <LoginButton />
        <LogoutButton />
      </nav>
    );
  }
}

export default ToggleButton;

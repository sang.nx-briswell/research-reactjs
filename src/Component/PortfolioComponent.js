import React, { Component } from "react";
import axios from "axios";

class Portfolio extends Component {
  state = {
    info: [],
  };
  componentDidMount = () => {
    const url =
      "https://en.wikipedia.org/w/api.php?action=opensearch&search=Black+Hole&format=json&origin=*";

    axios.get(url).then((res) => {
      const info = res.data;
      this.setState({
        info: info,
      });
    });
  };

  render() {
    return (
      <ul>
        {this.state.info.map((info, index) => (
          <li key={index}>{info}</li>
        ))}
      </ul>
    );
  }
}

export default Portfolio;
